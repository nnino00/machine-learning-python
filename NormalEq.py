import numpy as np
import pandas as pd

def solve():
    training_data = pd.read_csv('ex1data2.txt', names=["roomsize", "bedrooms", "price"])
    x = getDataX(training_data)
    data_y = getDataY(training_data)
    theta = normalEquation(x, data_y)
    print(theta)
    price = np.array([1, 1650, 3]).dot(theta)
    print('Predicted Price: ', price)

def addOneColumn(datas):
    ones = np.ones((datas.shape[0], 1), dtype=int)
    x = np.concatenate((ones,datas), axis=1)
    return x

def getDataX(datas):
    cols = len(datas.columns)
    data_x = datas.iloc[:, 0:cols-1]  #first cols-1 data with all rows
    return data_x.values

def getDataY(datas):
    cols = len(datas.columns)
    y = datas.iloc[:, cols-1:cols]
    return y.values  #converts to array

def normalEquation(x, data_y):
    #theta = (transposed_x * x)^-1*transposed_x*y
    data_x = addOneColumn(x)
    xTx = data_x.T.dot(data_x)
    x_inverse = np.linalg.inv(xTx)  #inverse
    XtX_xT = x_inverse.dot(data_x.T)
    theta = XtX_xT.dot(data_y)

    return theta

solve()
class Matrix:

    def __init__(self, value):
        self.value = value

    def __repr__(self):
        return "Matrix('{}')".format(self.value)

    def __add__(self, other):
        rows = len(self.value)
        cols = len(other.value[0])
        result = self.makeMatrix(rows, cols)

        if (rows != len(other.value) or cols != len(self.value[0])):
            print('Error: The matirces cannot be added! Please check number of rows and columns')
        else :
            for i in range(rows):
                for j in range(cols):
                    result[i][j] = self.value[i][j] + other.value[i][j]

        return Matrix(result)

    def __sub__(self, other):
        rows = len(self.value)
        cols = len(other.value[0])
        result = self.makeMatrix(rows, cols)

        if (rows != len(other.value) or cols != len(self.value[0])):
            print('Error: The matirces cannot be subtracted! Please check number of rows and columns')
        else:
            for i in range(rows):
                for j in range(cols):
                    result[i][j] = self.value[i][j] - other.value[i][j]

        return Matrix(result)

    def __mul__(self, other):
        a = self.value
        b = other.value

        rowA = len(a)
        colA = len(a[0])
        rowB = len(b)
        colB = len(b[0])

        result = self.makeMatrix(rowA, colB)
        
        if (colA != rowB) :
            print('Error: The matirces cannot be multiplied! Please check number of rows and columns')
        else :
            for i in range(rowA):
                for j in range(colB):
                    for k in range(rowB):
                        result[i][j] += self.value[i][k] * other.value[k][j]
                   
        return Matrix(result)
    
    def __pow__(self, number):
        rows = len(self.value)
        cols = len(self.value[0])
        result = self.makeMatrix(rows, cols)

        for i in range(rows):
            for j in range(cols):
                result[i][j] = self.value[i][j] ** number
        return Matrix(result)
        
    def transpose(self):
        rows = len(self.value)
        cols = len(self.value[0])
        result = self.makeMatrix(cols, rows)
       
        for i in range(rows):
            for j in range(cols):
                result[j][i] = self.value[i][j]
        return Matrix(result)

    def makeMatrix(self, rows, cols):
        a= [0] * rows
        for i in range (rows):
            a[i] = [0] * cols
        return a


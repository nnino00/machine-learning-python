import numpy as np
import pandas as pd

def solve():
    datas = pd.read_csv('ex1data2.txt', names=["roomsize", "bedrooms", "price"])
    a = normalizeData(datas)
    x = getDataX(a)
    data_y = getDataY(datas)
    data_x = addOneColumn(x)
    theta = thetas(a)
    g,c = gradientDescent(data_x, data_y, theta, 1000, 0.01)
    print('thetas:', g)
    size_mean = datas.mean().roomsize
    size_std = datas.std().roomsize

    area = (1650 - size_mean)/size_std
    room_mean = datas.mean().bedrooms
    room_std = datas.std().bedrooms
    room = (3 - room_mean)/room_std

    price = np.array([1, area, room]).dot(g.T)
    print('Predicted Price:', price)

def normalizeData(datas):
    return (datas - datas.mean())/datas.std()

def addOneColumn(datas):
    ones = np.ones((datas.shape[0], 1), dtype=int)
    x = np.concatenate((ones,datas), axis=1)
    return x

def getDataX(datas):
    cols = len(datas.columns)
    data_x = datas.iloc[:, 0:cols-1]  #first cols-1 data with all rows
    return data_x.values

def getDataY(datas):
    cols = len(datas.columns)
    y = datas.iloc[:, cols-1:cols]  
    return y.values  #converts to array

def thetas(datas):
    cols = len(datas.columns)  #n+1
    res = np.zeros([1, cols])
    return res

def computeCost(x, y, theta):
    tobesummed = np.power(((x @ theta.T)-y),2)
    return np.sum(tobesummed)/(2 * len(x))

def gradientDescent(x, y, theta, iters, alpha):
    cost = np.zeros(iters)
    for i in range(iters):
        theta = theta - (alpha/len(x)) * np.sum(x * (x @ theta.T - y), axis=0)  #1/m summation(hyp(x^i) - y^i).x.j^i
        cost[i] = computeCost(x, y, theta)

    return theta, cost

solve()